import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static String encrypt(String inputMsg) {
        String encryptionKey = "VENEZIA";
        String res = "";
        inputMsg = inputMsg.toUpperCase();
        for (int i = 0, j = 0; i < inputMsg.length(); i++) {
            char c = inputMsg.charAt(i);
            if (c < 'A' || c > 'Z')
                continue;
            res += (char) ((c + encryptionKey.charAt(j) - 2 * 'A') % 26 + 'A');
            j = ++j % encryptionKey.length();
        }
        return res;
    }

    public static void main(String[] args) throws IOException {
        String tmp;
        String encryptedMsg = "";
        Scanner myScan = new Scanner(System.in);

        // Connessioni client -> servere
        System.out.println("CLIENT-> CONNESSIONE VERSO UN SERVER...");
        Socket s = new Socket("127.0.0.1", 3000);
        System.out.println(
                "CLIENT-> CONNESSIONE AVVENUTA VERSO " + s.getInetAddress() + " ALLA PORTA " + s.getLocalPort());

        // richiesta dati all'utente
        System.out.println("CLIENT-> INSERIRE IL MESSAGGIO DA CRITTOGRAFARE");
        tmp = myScan.nextLine();
        encryptedMsg = encrypt(tmp);
        // invia dati al server
        PrintWriter out = new PrintWriter(s.getOutputStream(), true);
        out.println(encryptedMsg);

        // riceve una risposta dal server
        InputStreamReader isr = new InputStreamReader(s.getInputStream());
        BufferedReader in = new BufferedReader(isr);
        System.out.println("SERVER-> RISPONDE: \n" + in.readLine());

        s.close();
    }
}
