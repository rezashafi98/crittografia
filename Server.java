
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static String decrypt(String msg) {
        String decryptionKey = "VENEZIA";
        String res = "";
        msg = msg.toUpperCase();
        for (int i = 0, j = 0; i < msg.length(); i++) {
            char c = msg.charAt(i);
            if (c < 'A' || c > 'Z')
                continue;
            res += (char) ((c - decryptionKey.charAt(j) + 26) % 26 + 'A');
            j = ++j % decryptionKey.length();
        }
        return res;
    }

    public static void main(String[] args) throws IOException {

        String recievedMsg = "";
        String decryptedMsg = "";

        // istanza connessione
        ServerSocket ss = new ServerSocket(3000);

        while (true) {

            System.out.println("SERVER-> IN ATTESA DI CONNESSIONI ....");
            // riceve una connessione
            Socket s = ss.accept();
            System.out.println("SERVER-> CONNESSO CON " + s.getInetAddress() + " ALLA PORTA REMOTA " + s.getPort()
                    + " E ALLA PORTA LOCALE " + s.getLocalPort());

            // ricezione e decriptaggio
            InputStreamReader isr = new InputStreamReader(s.getInputStream());
            BufferedReader in = new BufferedReader(isr);
            recievedMsg = in.readLine();
            decryptedMsg = decrypt(recievedMsg);
            System.out.println("SERVER-> RICEVO: " + recievedMsg);
            System.out.println("SERVER-> DECRIPTO E INVIO " + decryptedMsg);

            // invia del testo
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            out.println("MESSAGGIO DECRIPTATO DAL SERVER: " + decryptedMsg);

            s.close();
        }

    }
}
